﻿/*
 Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.md or http://ckeditor.com/license
 */

/**
 * @file Plugin for inserting embeded media
 */
( function() {
	CKEDITOR.plugins.add( 'mediavideo',
	{
		// Translations, available at the end of this file, without extra requests
		lang : [ 'en', 'es' ],

		getPlaceholderCss : function()
		{
			return 'img.cke_mediavideo' +
					'{' +
						'background-image: url(' + CKEDITOR.getUrl( this.path + 'images/placeholder.png' ) + ');' +
						'background-position: center center;' +
						'background-repeat: no-repeat;' +
						'background-color:gray;'+
						'border: 1px solid #a9a9a9;' +
						'width: 80px;' +
						'height: 80px;' +
					'}';
		},

		onLoad : function()
		{
			// v4
			if (CKEDITOR.addCss)
				CKEDITOR.addCss( this.getPlaceholderCss() );

		},
		init: function( editor )
		{
			var lang = editor.lang.mediavideo;

			if (typeof editor.element.data == 'undefined')
			{
				alert('The "video" plugin requires CKEditor 3.5 or newer');
				return;
			}

			CKEDITOR.dialog.add( 'mediavideo', this.path + 'dialogs/video.js' );

			editor.addCommand( 'MediaVideo', new CKEDITOR.dialogCommand( 'mediavideo' ) );
			
			editor.ui.addButton( 'mediavideo',
			{
				label: 'mediavideo',
				command: 'mediavideo',
				icon: this.path + 'images/icon.png',
				toolbar: 'wordpress,100'
			} );
		}
	} );




var en = {
		toolbar	: 'Video',
		dialogTitle : 'Video properties',
		fakeObject : 'Video',
		properties : 'Edit video',
		widthRequired : 'Width field cannot be empty',
		heightRequired : 'Height field cannot be empty',
		poster: 'Poster image',
		sourceVideo: 'Source video',
		sourceType : 'Video type',
		linkTemplate :  '<a href="%src%">%type%</a> ',
		fallbackTemplate : 'Your browser doesn\'t support video.<br>Please download the file: %links%'
	};

var es = {
		toolbar	: 'Video',
		dialogTitle : 'Propiedades de video',
		fakeObject : 'Video',
		properties : 'Editar el video',
		widthRequired : 'La anchura no se puede dejar en blanco',
		heightRequired : 'La altura no se puede dejar en blanco',
		poster: 'Imagen de presentación',
		sourceVideo: 'Archivo de video',
		sourceType : 'Tipo',
		linkTemplate :  '<a href="%src%">%type%</a> ',
		fallbackTemplate : 'Su navegador no soporta VIDEO.<br>Por favor, descargue el fichero: %links%'
	};

	// v3
	if (CKEDITOR.skin)
	{
		en = { mediavideo : en} ;
		es = { mediavideo : es} ;
	}

// // Translations
CKEDITOR.plugins.setLang( 'mediavideo', 'en', en );

CKEDITOR.plugins.setLang( 'mediavideo', 'es', es );

})();
