<!DOCTYPE html>
<html>

<head>
    <title>Grupo Paula Bellotti <?php wp_title(); ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/beauty-font.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/index.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/responsive.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.nicescroll.min.js"></script>
    <script src="http://markdalgleish.com/projects/stellar.js/js/stellar.js"></script>
    <script src="https://ihatetomatoes.net/demos/scrolling-libraries/js/scrollReveal.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery-pagination-scroll.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/index.js"></script>
    <?php wp_head(); ?>
</head>

<body>
<div class="background" data-stellar-background-ratio="0.5" style="background-image: url(<?php bloginfo('template_url'); ?>/images/<?php echo _BAN_; ?>);">
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">             
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span> 
              </button>
              <i class="fa fa-search visible-sm visible-xs"></i>
              <a class="navbar-brand" href="<?php bloginfo('url'); ?>">
                  <img src="<?php bloginfo('template_url'); ?>/images/log-pb.png">
              </a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <?php
                    wp_nav_menu( array(
                        'theme_location'              => 'primary',
                        'menu_class'       => 'nav navbar-nav',
                        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth'             => 0,
                        'container'       => '',
                        'container_class' => false,
                        
                    ));
                ?>
              <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="https://www.facebook.com/paulabellotti" target="_blank">
                        <i class="icon icon-social-facebook"></i>
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com/PaulaBellotti" target="_blank">
                        <i class="icon icon-social-twitter"></i>
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/grupopaulabellotti/" target="_blank">
                        <i class="icon icon-social-instagram"></i>
                    </a>
                </li>
                <li class="hidden-sm hidden-xs">
                    <a href="#">
                        <i class="fa fa-search"></i>
                    </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <div class="form-search">
            <div class="container">
                <form method="get" action="<?php bloginfo('url'); ?>">
                    <input name="s" value="<?php the_search_query(); ?>" type="text" class="form-control" placeholder="Faça sua busca">
                </form>
            </div>
        </div>