<?php define(_BAN_, 'bg3.jpg'); get_header(); ?>
<section>
    <div class="container">
       <?php paulabellotti_breadcrumbs(); ?>

        <h2><?php single_term_title(); ?></h2>

       <form>
        <div class="row">
            <div class="col-sm-2">
                <div class="form-group">
                    <select class="form-control" name="ano" onchange="submit()">
                        <option value="">Ano</option>
                            
                        <?php for($i=date('Y'); $i>=2012; $i--){ ?>
                        <option value="<?php echo $i; ?>" <?php selected( $_GET['ano'], $i ); ?>><?php echo $i; ?></option>
                        <?php } ?>

                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <select class="form-control text-uppercase" name="mes" onchange="submit()">
                        <option value="">Mês</option>
                        <option value="01" <?php selected( $_GET['mes'], '01' ); ?>>Janeiro</option>
                        <option value="02" <?php selected( $_GET['mes'], '02' ); ?>>Fevereiro</option>
                        <option value="03" <?php selected( $_GET['mes'], '03' ); ?>>Março</option>
                        <option value="04" <?php selected( $_GET['mes'], '04' ); ?>>Abril</option>
                        <option value="05" <?php selected( $_GET['mes'], '05' ); ?>>Maio</option>
                        <option value="06" <?php selected( $_GET['mes'], '06' ); ?>>Junho</option>
                        <option value="07" <?php selected( $_GET['mes'], '07' ); ?>>Julho</option>
                        <option value="08" <?php selected( $_GET['mes'], '08' ); ?>>Agosto</option>
                        <option value="09" <?php selected( $_GET['mes'], '09' ); ?>>Setembro</option>
                        <option value="10" <?php selected( $_GET['mes'], '10' ); ?>>Outubro</option>
                        <option value="11" <?php selected( $_GET['mes'], '11' ); ?>>Novembro</option>
                        <option value="12" <?php selected( $_GET['mes'], '12' ); ?>>Dezembro</option>
                    </select>
                </div>
            </div>
        </div>
       </form>


<?php

$ano = '[0-9]{4}';
$mes = '[0-9]{2}';

if (isset($_GET['ano'])) {
    $ano = $_GET['ano'];
}

if (isset($_GET['mes'])) {
    $mes = $_GET['mes'];
}

$args = array (
  'post_type'      => 'imprensa',      
  'meta_key'       => 'data_veiculacao',  
  'orderby'        => 'meta_value_num',
  'order'          => 'DESC',
  'meta_query' => array(
    array(
      'key'      => 'data_veiculacao',
      'compare'  => 'REGEXP',
      'value'    => $ano . $mes . '[0-9]{2}',
    ),    
  )
);

$imprensa = new WP_Query($args);
?>


        <div class="row clippings">
            <?php $c=0; while($imprensa->have_posts()): $imprensa->the_post(); $c++; ?>
            <div class="col-md-3 col-sm-4 col-xs-6">
                <a href="<?php echo get_field('arquivo'); ?>" class="clipping" download>
                   <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'thumbnail')[0]; ?>">
                        <p><b>Data:</b> <br> <?php echo format_date_br(get_field('data_veiculacao')); ?></p>
                        <p><b>Veículo: </b> <br> <?php echo get_field('fonte'); ?></p>
                        <p><b>Assunto:</b> <br> <?php echo get_field('assunto'); ?></p>
                    <span><i class="icon icon-basic-download"></i> fazer download</span>
                </a>
            </div>
            <?php echo $c%4==0 ? '</div><div class="row clippings">' : null; endwhile; ?>
        </div>


        <div class="pagination-box">
        <div class="pagination">
            <?php echo paginate_links(); ?>
        </div>
     
        </div>
    </div>
</section>    
<?php get_footer(); ?>