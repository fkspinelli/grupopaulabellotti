<?php define(_BAN_, 'bg3.jpg'); get_header(); ?>
<section>
    <div class="container">
        <?php paulabellotti_breadcrumbs(); ?>
        
        <!-- definindo destaque -->
        <?php $destaque = get_post('39'); //print_r($destaque); ?>
        <div class="row">
            <div class="col-sm-7">
                <div>
                    <h2><?php echo $destaque->post_title; ?></h2>
                    <?php echo $destaque->post_content; ?>
                </div>
                <div class="diver danger"></div>
            </div>
            <div class="col-sm-5">
                <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($destaque->ID),'full')[0]; ?>" class="img-thumbnail">
            </div>
        </div>
        
        <?php $terms = get_terms( array(
                'taxonomy' => 'categoria_equipe',
                'parent' => 0,
                'hide_empty' => false,
                ) ); 
        foreach ($terms as $term) :
        ?>
        <div class="equipe">
            <h2><?php echo $term->name; ?></h2>
            <?php 
                $termchildren = get_term_children( $term->term_id, 'categoria_equipe' );
                if(!empty($termchildren)){
                    foreach ( $termchildren as $c ) {
                        $term = get_term_by( 'id', $c, 'categoria_equipe' );
                        echo '<h3>'.$term->name.'</h3>'; ?>
                        <div class="row">
                            <?php $args = array('post_type'=>'equipe', 'tax_query' => array(
                                        array(
                                            'taxonomy' => 'categoria_equipe',
                                            'field' => 'slug',
                                            'terms' => $term->slug
                                        )
                                    )); 
                            query_posts($args); $c=0;
                            while(have_posts()) : the_post(); $c++;
                            ?>
                            <div class="col-sm-4">
                                <div class="profile">
                                    <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'thumb_square')[0]; ?>" class="img-thumbnail">
                                    <h4><?php the_title(); ?></h4>
                                    <?php the_content(); ?>
                                </div>
                            </div>
                            <?php echo $c%3==0 ? '</div><div class="row">' : null;  endwhile; wp_reset_query(); ?>
                        </div>
                <?php    }
                }else{ ?>
                   <div class="row">
                            <?php $args = array('post_type'=>'equipe', 'tax_query' => array(
                                        array(
                                            'taxonomy' => 'categoria_equipe',
                                            'field' => 'slug',
                                            'terms' => $term->slug
                                        )
                                    )); 
                            query_posts($args); $j=0;
                            while(have_posts()) : the_post(); $j++;
                            ?>
                            <div class="col-sm-4">
                                <div class="profile">
                                    <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'thumb_square')[0]; ?>" class="img-thumbnail">
                                    <h4><?php the_title(); ?></h4>
                                    <?php the_content(); ?>
                                </div>
                            </div>
                            <?php  echo $j%3==0 ? '</div><div class="row">' : null;  endwhile; wp_reset_query(); ?>
                        </div>   
             <?php   }
            ?>

            <!-- <div class="row">
                <div class="col-sm-4 col-sm-push-4">
               
                    <a href="<?php echo get_field('link_clinica',$term); ?>" class="btn btn-danger btn-block text-uppercase">acessar <?php echo $temr->name; ?></a>
                </div>
            </div> -->
        </div>
        <?php endforeach; ?>
       

    </div>
</section>  
<?php get_footer(); ?>