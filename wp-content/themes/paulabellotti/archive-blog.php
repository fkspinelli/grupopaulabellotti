<?php define(_BAN_, 'bg3.png'); get_header(); ?>
<section>
    <div class="top-blog">
        <div class="container">
            <div style="position: absolute; width: 100%;">
                <?php paulabellotti_breadcrumbs(); ?>
            </div>
            <div class="row">
                <div class="col-sm-9 col-sm-push-3 text">
                    <h4>Blog</h4>
                    <h5>Questão de Pele</h5>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="posts">
                <?php while(have_posts()): the_post(); ?>
                    <div class="panel panel-default artigo eqh materia">
                        <div class="image-post" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'medium')[0]; ?>);">
                            <img src="<?php echo bloginfo('template_url') ?>/images/post-blog-height.jpg">
                        </div>
                        <div class="panel-body">
                            <h5><strong><?php echo get_the_date('d F Y'); ?> </strong> <?php echo get_field('clinica')->post_title; ?></h5>
                            <h3><?php the_title(); ?></h3>
                            <p><?php echo strip_tags(get_the_excerpt()); ?></p>
                        </div>
                        <div class="panel-footer">
                            <a href="<?php the_permalink(); ?>" class="btn btn-default link-icon text-uppercase pull-left">continuar lendo</a>
                            <!-- <div class="pull-right">
                                <div class="btn-group pull-left">
                                  <a href="#" class="btn btn-default"><i class="fa fa-facebook"></i></a>
                                  <a href="#" class="btn btn-default twitter"><i class="fa fa-twitter"></i></a>
                                  <a href="#" class="btn btn-default"><i class="fa fa-tumblr"></i></a>
                                  <a href="#" class="btn btn-default"><i class="fa fa-google-plus"></i></a>
                                </div>
                                <div class="btn-group pull-left">
                                  <a href="#" class="btn btn-danger"><i class="fa fa-share-alt"></i></a>
                                </div>
                            </div> -->
                        </div>
                    </div>
                <?php endwhile; ?>
                <div class="pagination col-sm-6 col-sm-push-3">
                     <?php echo get_next_posts_link("ver mais matérias"); ?>
                </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-sm-push-3 ">
                      <?php //echo paginate_links(); ?>
                       <!-- <a href="#" class="btn btn-danger btn-block text-uppercase" style="margin-bottom: 50px;">ver mais matérias</a> -->
                    </div>
                </div>
            </div>
            <?php get_template_part('partials/content', 'sidebar'); ?>
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){
        $('.pagination a').addClass('btn btn-danger btn-block text-uppercase');
        $('.posts').jscroll({
            autoTrigger: false,
            nextSelector: '.pagination a',
            contentSelector: '.materia',
            loadingHtml: '<span class="text-warning">Carregando...</span>',
            callback: function(){

                var artigoheight = 0;
                $('.artigo.eqh .panel-body').each(function(){
                    if($(this).height() > artigoheight){
                        artigoheight = $(this).height();
                    }
                });
                $('.artigo.eqh .panel-body').css({'min-height':artigoheight});

            }
        });
    });
</script>
<?php get_footer(); ?>