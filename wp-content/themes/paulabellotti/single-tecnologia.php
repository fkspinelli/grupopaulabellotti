<?php define(_BAN_, 'bg3.jpg'); get_header(); the_post(); ?>
<section>
    <div class="container">
        <?php paulabellotti_breadcrumbs(); ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="row select-servico">
                    <div class="col-sm-7">
                        <h2>Centro Avançado de Laser</h2>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <select id="select_tec" class="form-control">
                                <option value="">Tecnologias</option>
                                <?php $comp = $post->ID; $args = array('post_type'=>'tecnologia', 'posts_per_page'=>-1); query_posts($args); while(have_posts()): the_post(); ?>
                                    <option <?php echo $comp == $post->ID ? 'selected' : null; ?> value="<?php the_permalink(); ?>"><?php the_title(); ?></option>
                                <?php endwhile; wp_reset_query(); ?>
                            </select>
                        </div>
                    </div>
                </div>
                <h6><?php the_title(); ?></h6>
                <div class="tec-content">
                    <?php the_content(); ?>
                </div>

                <?php if(get_field('video_src')){ ?>
                <div class="row">
                    <div class="col-sm-8 col-sm-push-2">
                        <video controls class="video-tec">
                            <source src="<?php echo get_field('video_src'); ?>" type="video/mp4">
                        </video> 
                    </div>
                </div>
                <?php }?>

                <div class="trat">
                    <h5>Tratamentos que utilizam</h5>
                    <ul>
                        <li>
                            <a href="#">Corporal <i class="icon icon-arrows-slim-right"></i></a>
                        </li>
                        <li>
                            <a href="#">Facial <i class="icon icon-arrows-slim-right"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div> 
        <?php //$clinicas = get_field('onde_encontrar'); if(!empty($clinicas)): ?>  
        <!-- <div class="onde-encontrar">
            <h3>Onde encontrar</h3>
            <div class="row">
                <?php foreach ($clinicas as $k => $clinica): ?>
                <div class="col-sm-2">
                    <a href="<?php echo get_permalink($clinica->ID); ?>">
                        <img draggable="false" src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($clinica->ID),'full')[0]; ?>">
                    </a>
                </div>
               <?php  endforeach; ?>
            </div>
        </div> -->
    <?php //endif; ?>
    </div>
</section>
<!-- <div style="margin-bottom: 100px;"></div> -->
<?php include( locate_template( 'partials/content-relacionados.php', false, false ) ); ?>
<?php get_footer(); ?>