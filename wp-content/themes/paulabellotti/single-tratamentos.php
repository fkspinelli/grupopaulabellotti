<?php define(_BAN_, 'bg2.jpg'); get_header(); the_post(); include( locate_template( 'partials/content-banner-tratamentos.php', false, false ) ); ?>
<div class="breadcrumbs-banner">
    <div class="container">
        <?php paulabellotti_breadcrumbs(); ?>
    </div>
</div>
<section style="padding-top: 100px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-7">
                <div class="content">
                    <h2><?php the_title(); ?></h2>
                   <?php the_content(); ?>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="clearfix">
                    <?php include( locate_template('partials/content-social.php', false, false)); ?>
                </div>
                <div class="position-relative" style="max-width: 400px;">
                    <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'thumb_square')[0]; ?>" class="img-thumbnail">
                    <?php if (!empty(get_field('video'))): ?>
                        <img src="<?php echo bloginfo('template_url'); ?>/images/icon-play.png" class="icon-play" data-toggle="modal" data-target="#modalTratamento">
                    <?php endif; ?>
                </div>
            </div>
        </div>
       <?php $tecs = get_field('tecnologias'); if(!empty($tecs)): ?>
        <div class="tratamentos-btns">
            <h2>Tratamentos</h2>
            <div class="row">
                <?php foreach ($tecs as $tec): ?>
                <div class="col-md-3 col-sm-4">
                    <a href="<?php echo get_permalink($tec->ID); ?>" class="btn btn-pink-2 btn-block text-uppercase"><span><?php echo $tec->post_title; ?></span></a>
                </div>
                <?php endforeach; endif; ?>
            </div>
        </div>
    </div>
</section>
<div style="margin-bottom: 20px;"></div>
<?php include( locate_template( 'partials/content-relacionados.php', false, false ) ); ?>

<!-- Modal -->
<div id="modalTratamento" class="modal fade" role="dialog">
  <div class="modal-dialog" style="margin-top: 100px;">
    <iframe src="https://www.youtube.com/embed/<?php echo explode('watch?v=', get_field('video'))[1]; ?>" frameborder="0" allowfullscreen style="width: 100%; height: 338px;"></iframe>
  </div>
</div>
<?php get_footer(); ?>