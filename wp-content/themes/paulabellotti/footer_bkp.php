 <footer>
            <div class="container">
                <div class="row">
                    <div class="col-sm-11 col-sm-push-1">
                        <div class="news">
                            <h4>Receba as novidades por e-mail e fique por dentro de tudo o que acontece no Grupo Paula Bellotti.</h4>
                            <div class="row">
                                <?php echo do_shortcode('[contact-form-7 id="21" title="Formulário Newsletter"]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 col-sm-push-3">
                            <a href="#" class="logo-grupo-paula-bellotti">
                                <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/grupo-paula-bellotti.png" class="logo-grupo-paula-bellotti">
                            </a>
                          <?php
                        wp_nav_menu( array(
                            'theme_location' => 'secundary',
                            'menu_class' =>'footer_menu'
                            ));
                        ?>
                        </div>
                        <div class="col-sm-6 col-sm-push-3">
                            <div class="row">
                                <div class="social-networks">
                                    <ul>
                                        <li>
                                            <a href="https://www.facebook.com/paulabellotti" target="_blank"><i class="icon icon-social-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://twitter.com/PaulaBellotti" target="_blank"><i class="icon icon-social-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/grupopaulabellotti/" target="_blank"><i class="icon icon-social-instagram"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8 col-sm-push-4">
                                    <form>
                                        <div class="search">
                                            <div class="inner-addon right-addon">
                                                <button><i class="icon icon-basic-magnifier"></i></button>
                                                <input type="text" class="form-control" placeholder="Faça sua busca">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <div class="clinicas-do-grupo">
                                    <div class="col-xs-4">
                                        <a href="<?php bloginfo('url'); ?>/clinicas/clinica-paula-bellotti/">
                                            <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/logo-paula-bellotti-transparent.png" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="col-xs-4">
                                        <a href="<?php bloginfo('url'); ?>/clinicas/the-skin/">
                                            <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/logo-the-skin-transparent.png" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="col-xs-4">
                                        <a href="<?php bloginfo('url'); ?>/clinicas/instituto-da-pelle/">
                                            <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/logo-instituto-da-pele-transparent.png" class="img-responsive">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="creditos">
                        <div class="row">
                            <div class="col-sm-3">
                                <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/paulinha.png" class="paulinha">
                            </div>
                            <div class="col-sm-9">
                                <a href="http://www.dizain.com.br/" target="blank">
                                    <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/dizainicon.png">
                                </a>
                                <p>2017 - @grupo paula belloti</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</body>

</html>
