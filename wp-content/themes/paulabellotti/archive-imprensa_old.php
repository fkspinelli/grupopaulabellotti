<?php define(_BAN_, 'bg3.jpg'); get_header(); ?>
<section>
    <div class="container">
       <?php paulabellotti_breadcrumbs(); ?>
        
        <?php $terms = get_terms(array(
                'taxonomy' => 'categoria_imprensa',
                'parent' => 0,
                'hide_empty' => false,
                )); ?>
        <?php foreach ($terms as $term): ?>

        <h2><?php echo $term->name; ?></h2>
        <div class="row clippings">
            <?php $args = array('post_type'=>'imprensa', 'posts_per_page'=>'4', 'tax_query' => array(
                                        array(
                                            'taxonomy' => 'categoria_imprensa',
                                            'field' => 'slug',
                                            'terms' => $term->slug
                                        )
                                    )); 
                            query_posts($args);
                while(have_posts()) : the_post(); 
            ?>
            <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="clipping">
                    <a href="<?php echo get_field('arquivo'); ?>" target="_blank">
                        <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'thumbnail')[0]; ?>">
                        <p><b>Data:</b> <br> <?php echo get_field('data_veiculacao'); ?></p>
                        <p><b>Veículo: </b> <br> <?php echo get_field('fonte'); ?></p>
                        <p><b>Assunto:</b> <br> <?php echo get_field('assunto'); ?></p>
                    </a>
                    <a href="<?php echo get_field('arquivo'); ?>" download="">
                        <span><i class="icon icon-basic-download"></i> fazer download</span>
                    </a>
                </div>
            </div>
            <?php endwhile; wp_reset_query(); ?>
        </div>
        <a href="<?php echo get_term_link($term->term_id, 'categoria_imprensa'); ?>" class="todos-clippings">Acessar todos</a>
        <?php endforeach ?>
    </div>
</section>    
<?php get_footer(); ?>