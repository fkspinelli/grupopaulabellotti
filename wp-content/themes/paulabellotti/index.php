<?php define(_BAN_, 'bg1.png'); get_header(); get_template_part('partials/content', 'banner'); ?>

<?php get_template_part('partials/content', 'blog'); ?>

<section>
    <div class="container">
        <div class="row">
            <?php $destaque = get_post('39'); ?>
            <div class="col-sm-12">
                <div class="artigo dra box">
                    <div class="row">
                        <div class="col-sm-2">
                            <!-- <div class="foto hidden-xs" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($destaque->ID),'medium')[0]; ?>);"></div> -->
                            <div class="foto hidden-xs" style="background-image: url(<?php bloginfo('template_url'); ?>/images/paula-bellotti-2.png);"></div>
                            <img src="<?php bloginfo('template_url'); ?>/images/paula-bellotti-2.png" style="width: 100%; display: none; margin-bottom: 20px;" class="visible-xs">
                            <!-- <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($destaque->ID),'medium')[0]; ?>" style="width: 100%; display: none; margin-bottom: 20px;" class="visible-xs"> -->
                        </div>
                        <div class="col-sm-10">
                            <h3><?php echo $destaque->post_title; ?></h3>
                            <p><?php echo $destaque->post_excerpt; ?></p>
                            <a href="<?php bloginfo('url'); ?>/equipe" class="link-icon">Perfil completo</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="videos">
            <h2>Vídeos</h2>
            <div class="panel panel-default">
                <div class="row">
                    <div class="col-sm-7">
                        <iframe src="https://www.youtube.com/embed/7tGtUfJJOVQ" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="col-sm-5">
                        <div class="list-videos">
                            <ul>
                                <li class="clearfix" data-video="7tGtUfJJOVQ">
                                    <div class="thumbnails" style="background-image:url(https://img.youtube.com/vi/7tGtUfJJOVQ/default.jpg);"></div>
                                    <div class="box-title">
                                        <h4>Paula Bellotti - Como tratar flacidez facial sem cirurgia</h4>
                                        <p>3 meses atrás</p>
                                    </div>
                                </li>
                                <li class="clearfix" data-video="aqKxUh-s1hY">
                                    <div class="thumbnails" style="background-image:url(https://img.youtube.com/vi/aqKxUh-s1hY/default.jpg);"></div>
                                    <div class="box-title">
                                        <h4>Paula Bellotti - Queda de cabelo</h4>
                                        <p>3 meses atrás</p>
                                    </div>
                                </li>
                                <li class="clearfix" data-video="S9VeBB2CWdE">
                                    <div class="thumbnails" style="background-image:url(https://img.youtube.com/vi/S9VeBB2CWdE/default.jpg);"></div>
                                    <div class="box-title">
                                        <h4>Paula Bellotti - Olheiras</h4>
                                        <p>4 meses atrás</p>
                                    </div>
                                </li>                           
                                <li class="clearfix" data-video="b6lJPHPOtUM">
                                    <div class="thumbnails" style="background-image:url(https://img.youtube.com/vi/b6lJPHPOtUM/default.jpg);"></div>
                                    <div class="box-title">
                                        <h4>Verdades e mitos sobre manchas com a Dra. Paula Bellotti</h4>
                                        <p>5 meses atrás</p>
                                    </div>
                                </li>
                                <li class="clearfix" data-video="ZGr8X47XICg">
                                    <div class="thumbnails" style="background-image:url(https://img.youtube.com/vi/ZGr8X47XICg/default.jpg);"></div>
                                    <div class="box-title">
                                        <h4>Paula Bellotti - Fios de Sustentação</h4>
                                        <p>9 meses atrás</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-sm-push-4">
                    <a href="#" target="_blank" class="btn btn-danger btn-block text-bold text-uppercase">acessar o canal no youtube</a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
