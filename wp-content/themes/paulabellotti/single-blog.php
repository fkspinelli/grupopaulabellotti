<?php define(_BAN_, 'bg3.jpg'); get_header(); the_post(); wpb_set_post_views(get_the_ID()); ?>
<section>
    <div class="top-blog">
        <div class="container">
            <div style="position: absolute; width: 100%;">
                <?php paulabellotti_breadcrumbs(); ?>
            </div>
            <div class="row">
                <div class="col-sm-9 col-sm-push-3 text">
                    <h4>Blog</h4>
                    <h5>Questão de Pele</h5>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="blog-content">
                <div class="row">
                    <div class="col-sm-7">
                        <a href="javascript:history.back(-1);" class="voltar">Voltar</a>
                    </div>
                    <div class="col-sm-5">
                       <?php include( locate_template( 'partials/content-social.php', false, false ) ); ?>
                    </div>
                </div>                            
                <div class="artigo">
                    <h5><strong>29 junho 2016 </strong> The Skin</h5>
                </div>
                <h2><?php the_title(); ?></h2>
                            <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'normal')[0]; ?>">
                            <?php the_content(); ?>
                        </div>
                        <div class="assinatura">
                            <div>
                                <?php echo get_avatar(get_the_author_meta('ID')); ?> 
                            </div>
                            <div>
                                <h4><?php the_author(); ?></h4>
                                <p><?php the_author_meta('description'); ?></p>
                            </div>
                        </div>
            </div>
            <?php get_template_part('partials/content', 'sidebar'); ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>