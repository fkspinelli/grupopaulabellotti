<?php define(_BAN_, 'bg3.jpg'); get_header(); ?>
<section>
    <div class="container">
       <?php paulabellotti_breadcrumbs(); ?>

        <h2><?php single_term_title(); ?></h2>
       <div class="row">
            <div class="col-sm-2">
                <div class="form-group">
                    <select class="form-control">
                        <option>2016</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <select class="form-control text-uppercase">
                        <option>Novembro</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row clippings">
            <?php $c=0; while(have_posts()): the_post(); $c++; ?>
            <div class="col-md-3 col-sm-4 col-xs-6">
                <a href="<?php echo get_field('arquivo'); ?>" class="clipping" download>
                   <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'thumbnail')[0]; ?>">
                        <p><b>Data:</b> <br> <?php echo format_date_br(get_field('data_veiculacao')); ?></p>
                        <p><b>Veículo: </b> <br> <?php echo get_field('fonte'); ?></p>
                        <p><b>Assunto:</b> <br> <?php echo get_field('assunto'); ?></p>
                    <span><i class="icon icon-basic-download"></i> fazer download</span>
                </a>
            </div>
            <?php echo $c%4==0 ? '</div><div class="row clippings">' : null; endwhile; ?>
        </div>


        <div class="pagination-box">
        <div class="pagination">
            <?php echo paginate_links(); ?>
        </div>
     
        </div>
    </div>
</section> 
<?php get_footer(); ?>