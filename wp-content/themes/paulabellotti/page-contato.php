<?php define(_BAN_, 'bg1.jpg'); get_header(); the_post(); ?>
<section>
    <div class="container">
       <?php paulabellotti_breadcrumbs(); ?>

        <h2>Contato</h2>
        <div class="row contato">
            <div class="col-sm-6">
                <?php echo do_shortcode('[contact-form-7 id="4" title="Formulário de contato 1"]'); ?>
            </div>
            <div class="col-sm-6">
                <div class="panel panel-default border-top">
                    <div class="panel-body">
                        <div class="contato">
                           <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>