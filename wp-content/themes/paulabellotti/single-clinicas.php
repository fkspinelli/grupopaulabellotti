<?php define(_BAN_, 'bg1.png'); get_header(); the_post(); include( locate_template( 'partials/content-banner-clinicas.php', false, false ) ); ?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
               <?php the_content(); ?>
            </div>
        </div>
        
        <?php  include( locate_template( 'partials/content-tratamentos.php', false, false ) );  ?>
        
        <?php include( locate_template( 'partials/content-equipe.php', false, false ) ); ?>

        <?php include( locate_template( 'partials/content-clinica-imprensa.php', false, false ) ); ?>
        
         <?php $locais = get_field('localizacao'); if(!empty($locais)): ?>
        <div class="localizacao">
            <div class="row">
                <div class="col-sm-8">
                    <h2>Localização</h2>
                </div>
                <div class="col-sm-4">
                    <a href="<?php bloginfo('url'); ?>/contato" class="btn btn-danger btn-block text-uppercase">Fale conosco</a>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <ul class="row tabs tab-1">
                                <?php foreach ($locais as $k => $local) : ?>
                                <li class="col-sm-12 <?php echo $k == '0' ? 'active' : null; ?>">
                                    <a data-toggle="pill" href="#tab<?php echo $local->ID; ?>">
                                       <?php echo $local->post_content; ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                            </ul>
                            <div class="tab-content">
                            <?php foreach ($locais as $k => $local) : ?>
                                <div id="tab<?php echo $local->ID; ?>" class="tab-pane fade <?php echo $k == '0' ? 'in active' : null; ?>">
                                    <iframe src="<?php echo get_field('mapa', $local) ?>" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                                 <?php endforeach; ?>
                            </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    </div>
</section>
<?php get_footer(); ?>