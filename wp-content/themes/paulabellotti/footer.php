 <footer>
            <div class="container">
                <div class="row">
                    <div class="col-sm-11 col-sm-push-1">
                        <div class="news">
                            <h4>Receba as novidades por e-mail e fique por dentro de tudo o que acontece no Grupo Paula Bellotti.</h4>
                            <div class="row">
                                <?php echo do_shortcode('[contact-form-7 id="21" title="Formulário Newsletter"]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <div class="row">   
                        <div class="col-sm-5 col-sm-push-3">
                            <a href="#" class="logo-grupo-paula-bellotti">
                                <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/grupo-paula-bellotti.png" class="logo-grupo-paula-bellotti">
                            </a>                            
                        </div>
                        <div class="col-sm-4 col-sm-push-3">
                            <form method="get" action="<?php bloginfo('url'); ?>">
                                <div class="search">
                                    <div class="inner-addon right-addon">
                                        <button><i class="icon icon-basic-magnifier"></i></button>
                                        <input name="s" value="<?php the_search_query(); ?>" type="text" class="form-control" placeholder="Faça sua busca">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3 col-sm-push-3">
                          <?php
                        wp_nav_menu( array(
                            'theme_location' => 'footer_1',
                            'menu_class' =>'footer_menu'
                            ));
                        ?>
                        </div>
                        <div class="col-sm-3 col-sm-push-3">
                          <?php
                        wp_nav_menu( array(
                            'theme_location' => 'footer_2',
                            'menu_class' =>'footer_menu'
                            ));
                        ?>
                        </div>
                        <div class="col-sm-3 col-sm-push-3">
                          <?php
                        wp_nav_menu( array(
                            'theme_location' => 'footer_3',
                            'menu_class' =>'footer_menu'
                            ));
                        ?>
                        </div>
                    </div>
                    <div class="creditos">
                        <div class="row">
                            <div class="col-sm-3">
                                <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/paulinha.png" class="paulinha">
                            </div>
                            <div class="col-sm-3">
                                <a href="http://www.dizain.com.br/" target="blank">
                                    <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/dizainicon.png">
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <p>2017 - @grupo paula belloti</p>
                            </div>
                            <div class="col-sm-3">
                                <div class="social-networks">
                                    <ul>
                                        <li>
                                            <a href="https://www.facebook.com/paulabellotti" target="_blank"><i class="icon icon-social-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://twitter.com/PaulaBellotti" target="_blank"><i class="icon icon-social-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/grupopaulabellotti/" target="_blank"><i class="icon icon-social-instagram"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</body>

</html>
