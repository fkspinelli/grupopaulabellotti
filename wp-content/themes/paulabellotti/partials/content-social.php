<div class="social-network-b pull-right">
    <a onclick="javascript:window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=300'); return false;" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink(); ?>"><i class="icon icon-social-facebook"></i></a>
    <a onclick="javascript:window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=500,height=300'); return false;" href="https://twitter.com/home?status=<?php echo get_the_title().' - '.get_the_permalink(); ?>"><i class="icon icon-social-twitter"></i></a>
    <!--<a href="#" target="_blank"><i class="icon icon-social-instagram"></i></a> -->
</div>