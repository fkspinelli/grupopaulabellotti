<?php $banners = get_field('banners'); if(!empty($banners)): ?>
<section id="banner" class="carousel slide carousel-fade" data-ride="carousel">
    <div class="indicators">
        <div class="container">
            <div class="row">
                <div class="col-sm-11 col-sm-push-1 position-relative">
                    <ol class="carousel-indicators">
                    <?php foreach ($banners as $k => $banner): ?>
                         <li data-target="#banner" data-slide-to="<?php echo $k; ?>" class="<?php echo $k=='0' ? 'active' : null; ?>"></li>
                    <?php endforeach ?> 
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="carousel-inner" role="listbox">
        <?php foreach ($banners as $k => $banner): ?>
        <div class="item <?php echo $k=='0' ? 'active' : null; ?>" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($banner->ID),'full')[0]; ?>);">
            <div class="container position-relative">
                <div class="row">
                    <div class="col-sm-11 col-sm-push-1 position-relative">
                        <?php if($banner->post_content != ""){ ?>
                        <div class="text">
                            <span class="border"></span>
                            <p><?php echo $banner->post_content; ?></p>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="triangulo" data-stellar-background-ratio="0.5"></div>
            </div>
        </div>
        <?php endforeach ?>
    </div>
</section>
<?php endif; ?>