<div class="sobre-clinicas">
    <h2>CLÍNICAS DO GRUPO</h2>
    <div class="row">
        <div class="clinicas-do-grupo">
            <div class="col-xs-4">
                <a href="<?php bloginfo('url'); ?>/clinicas/clinica-paula-bellotti/" class="hover">
                    <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/logo-paula-bellotti-transparent.png" class="img-responsive">
                </a>
            </div>
            <div class="col-xs-4">
                <a href="<?php bloginfo('url'); ?>/clinicas/the-skin/" class="hover">
                    <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/logo-the-skin-2.png" class="img-responsive">
                </a>
            </div>
            <div class="col-xs-4">
                <a href="<?php bloginfo('url'); ?>/clinicas/instituto-da-pelle/" class="hover">
                    <img draggable="false" src="<?php bloginfo('template_url'); ?>/images/logo-instituto-da-pele-2.png" class="img-responsive">
                </a>
            </div>
        </div>
    </div>
</div>