<?php $equipe = get_field('equipe'); if(!empty($equipe)): ?>
<div class="equipe">
    <h2>Equipe</h2>
    <div class="row">
        <?php foreach($equipe as $k => $e): //print_r($e); ?>
        <div class="col-sm-4">
            <div class="profile">
                <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($e->ID),'thumb_square')[0]; ?>" class="img-thumbnail">
                <h4><?php echo $e->post_title; ?></h4>
               <?php echo $e->post_content; ?>
            </div>
        </div>
       <?php  if(($k+1)%3=='0'): echo '</div><div class="row">'; endif; endforeach; ?>
    </div>
</div>
<?php endif; ?>