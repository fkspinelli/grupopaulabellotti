<div class="col-sm-4">
    <div class="widget border-top">
        <h3>mais vistos</h3>
        <?php $popularpost = new WP_Query( array( 'posts_per_page' => 5, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC', 'post_type' =>'blog'  ) ); ?>
        <div>
          <?php while ( $popularpost->have_posts() ) : $popularpost->the_post();
                 ?>
            <a href="<?php the_permalink(); ?>" class="artigo">
                <div class="foto" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'medium')[0]; ?>);">
                    <img src="<?php echo bloginfo('template_url'); ?>/images/Layer-7.png">
                </div>
                <div>
                    <h5><strong><?php echo get_the_date('d F Y'); ?> </strong> <?php echo get_field('clinica')->post_title; ?></h5>
                    <h3><?php the_title(); ?></h3>
                </div>
            </a>
           <?php endwhile; wp_reset_postdata(); ?>
        </div>
    </div>
    <div class="widget border-top">
        <h3>assuntos</h3>
        <ul class="list-count">
            <?php $categories = wp_list_categories(array('taxonomy'=>'categoria_blog','hide_empty'=>false,'title_li'=>'','show_count'=>'true','echo'=>0));
                $categories = preg_replace('/<\/a> \(([0-9]+)\)/', ' <span class="count">(\\1)</span></a>', $categories);
                echo $categories;
            ?>
       
        </ul>
    </div>
    <div class="widget border-top">
        <h3>Instagram</h3>
        <div class="instagram">
            <ul></ul>
        </div>
    </div>
</div>