<?php $tratamentos = get_field('tratamentos'); if(!empty($tratamentos)): ?>
<div class="tratamentos">
    <h2>Tratamentos</h2>
    <div class="clearfix">
    	<?php foreach($tratamentos as $tratamento): ?>
        <a href="<?php echo esc_url( get_permalink($tratamento->ID) ); ?>" class="btn btn-pink btn-block btn-break text-uppercase btn-tratamentos"><span><?php echo $tratamento->post_title; ?></span></a>
       	<?php endforeach; ?>
    </div>
</div>
<?php endif; ?>