<section style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>Blog</h2>
            </div>
        </div>
        <?php $args = array('post_type' => 'blog', 'posts_per_page'=>'4'); query_posts($args); $c = 0; ?>
        <div class="row posts posts2">
            <?php while(have_posts()): the_post(); $c++; $img = wp_get_attachment_image_src(get_post_thumbnail_id(),'medium'); ?>
            <div class="col-sm-6">


                <div class="panel panel-default artigo eqh materia">
                    <div class="image-post" style="background-image: url(<?php echo $img[0]; ?>);">
                        <img src="<?php echo bloginfo('template_url'); ?>/images/Layer-7.png">
                    </div>
                    <div class="panel-body" style="min-height: 90px;">
                        <h5><strong><?php echo get_the_date('d F Y'); ?> </strong> <?php echo get_field('clinica')->post_title; ?></h5>
                        <h3 style="margin-bottom: 0;"><?php the_title(); ?></h3>
                        <!-- <p><?php the_excerpt(); ?></p> -->
                    </div>
                    <!-- <div class="panel-footer">
                        <a href="<?php the_permalink(); ?>" class="btn btn-default link-icon text-uppercase pull-left">continuar lendo</a>
                        <div class="pull-right">
                            <div class="btn-group pull-left">
                              <a href="#" class="btn btn-default"><i class="fa fa-facebook"></i></a>
                              <a href="#" class="btn btn-default twitter"><i class="fa fa-twitter"></i></a>
                              <a href="#" class="btn btn-default"><i class="fa fa-tumblr"></i></a>
                              <a href="#" class="btn btn-default"><i class="fa fa-google-plus"></i></a>
                            </div>
                            <div class="btn-group pull-left">
                              <a href="#" class="btn btn-danger"><i class="fa fa-share-alt"></i></a>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
            <?php if($c%4==0): echo '</div><div class="row">'; endif; endwhile; wp_reset_query(); ?>
        </div>
        
        
        <div class="row">
            <div class="col-sm-4 col-sm-push-4">
                <a href="<?php bloginfo('url'); ?>/blog" class="btn btn-danger btn-block text-bold text-uppercase">acessar o blog</a>
            </div>
        </div>
    </div>
</section>