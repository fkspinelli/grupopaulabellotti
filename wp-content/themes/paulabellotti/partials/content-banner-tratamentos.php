<?php $banners = get_field('banners'); if(!empty($banners)): ?>
<section id="banner" class="carousel slide carousel-fade home tratamento" data-ride="carousel">
    <img src="<?php echo bloginfo('template_url'); ?>/images/triangulo-top.png" class="triangulo-top">
    <div class="indicators">
        <div class="container">
            <div class="clearfix">
                <div class="position-relative text-center">
                    <ol class="carousel-indicators">
                    <?php foreach ($banners as $k => $banner): ?>
                         <li data-target="#banner" data-slide-to="<?php echo $k; ?>" class="<?php echo $k=='0' ? 'active' : null; ?>"></li>
                    <?php endforeach ?> 
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="carousel-inner" role="listbox">
        <?php foreach ($banners as $k => $banner): ?>
        <div class="item <?php echo $k=='0' ? 'active' : null; ?>" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($banner->ID),'full')[0]; ?>);">
            <a href="<?php echo $banner->link_externo; ?>" style="display: block;width: 100%;height: 100%;">
                <div class="container position-relative">
                    <div class="row">
                        <div class="col-sm-11 col-sm-push-1 position-relative">
                            <?php if($banner->post_content != ""){ ?>
                            <div class="text">
                                <span class="border"></span>
                                <p><?php echo $banner->post_content; ?></p>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <?php endforeach ?>
    </div>
</section>
<?php endif; ?>