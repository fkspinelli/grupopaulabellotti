<?php $posts = get_field('posts_blog'); if(!empty($posts)): ?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>Posts relacionados</h2>
            </div>
        </div>
        <div class="row posts posts2">
            <?php $cont =0; foreach ($posts as $k => $p): $cont++; ?>

            <div class="col-sm-6">
                <div class="panel panel-default artigo eqh2 materia">
                    <div class="image-post" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($p->ID),'medium')[0]; ?>);">
                        <img src="<?php echo bloginfo('template_url'); ?>/images/Layer-7.png">
                    </div>
                    <div class="panel-body" style="min-height: 90px;">
                        <h5><strong><?php //echo  ?> </strong> <?php echo get_field('clinica', $p)->post_title; ?></h5>
                        <h3><?php echo $p->post_title; ?></h3>
                        <!-- <p><?php echo $p->post_excerpt; ?></p> -->
                    </div>
                    <!-- <div class="panel-footer">
                        <a href="<?php the_permalink(); ?>" class="btn btn-default link-icon text-uppercase pull-left">continuar lendo</a>
                        <div class="pull-right">
                            <div class="btn-group pull-left">
                              <a href="#" class="btn btn-default"><i class="fa fa-facebook"></i></a>
                              <a href="#" class="btn btn-default twitter"><i class="fa fa-twitter"></i></a>
                              <a href="#" class="btn btn-default"><i class="fa fa-tumblr"></i></a>
                              <a href="#" class="btn btn-default"><i class="fa fa-google-plus"></i></a>
                            </div>
                            <div class="btn-group pull-left">
                              <a href="#" class="btn btn-danger"><i class="fa fa-share-alt"></i></a>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
            <?php echo $cont%2=='0' ? '</div><div class="row posts posts2">' : null; endforeach; ?>
        </div>

        <div class="row">
            <div class="col-sm-4 col-sm-push-4">
                <a href="<?php bloginfo('url'); ?>/blog" class="btn btn-danger btn-block text-bold text-uppercase">acessar o blog</a>
            </div>
        </div>
    </div>
</section> 
<?php endif; ?>