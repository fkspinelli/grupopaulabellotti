<?php $args = array('post_type'=>'banners','tax_query' => array(
                array(
                    'taxonomy' => 'categoria_banners',
                    'field' => 'slug',
                    'terms' => 'destaque'
                )
            )); 
            query_posts($args); 
?>
<section id="banner" class="carousel slide carousel-fade home" data-ride="carousel">
    <img src="<?php echo bloginfo('template_url'); ?>/images/triangulo-top.png" class="triangulo-top">
    <div class="indicators">
    	<div class="container">
        	<div class="clearfix">
        		<div class="position-relative text-center">
	            	<ol class="carousel-indicators">
                        <?php  $c=-1; while(have_posts()): the_post(); $c++; ?>
		                <li data-target="#banner" data-slide-to="<?php echo $c; ?>" class="<?php echo $c=='0' ? 'active' : null; ?>"></li>
		                <?php endwhile; ?>
		            </ol>
        		</div>
        	</div>
    	</div>
    </div>
    <div class="carousel-inner" role="listbox">
        <?php  $j=-1; while(have_posts()): the_post(); $j++; ?>
        <div class="item <?php echo $j=='0' ? 'active' : null; ?>" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'full')[0]; ?>);">
            <a href="<?php echo get_field('link_externo'); ?>" style="display: block;width: 100%;height: 100%;">
            	<div class="container" style="height: 100%;">
    				<div class="row" style="height: 100%;">
    					<div class="col-sm-6 col-sm-push-3 position-relative" style="height: 100%;">
    						<div class="text">
    	                		<span class="border"></span>
    	                		<?php the_content(); ?>
                    		</div>
    					</div>
    				</div>
            	</div>
            </a>
        </div>
        <?php endwhile; wp_reset_query(); ?>
    </div>
</section>