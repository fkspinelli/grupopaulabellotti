<?php $clippings = get_field('imprensa'); if(!empty($clippings)): ?>
<h2><?php the_title(); ?> na mídia</h2>
<div class="row clippings">
    <?php foreach ($clippings as $clipping): ?>
    
    <div class="col-md-3 col-sm-4 col-xs-6">
        <div class="clipping">
            <a href="<?php echo get_field('arquivo', $clipping); ?>" target="_blank">
                <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($clipping->ID),'thumbnail')[0]; ?>">
                <p><b>Data:</b> <br> <?php echo get_field('data_veiculacao', $clipping); ?></p>
                <p><b>Veículo: </b> <br> <?php echo get_field('fonte', $clipping); ?></p>
                <p><b>Assunto:</b> <br> <?php echo get_field('assunto', $clipping); ?></p>
            </a>
            <a href="<?php echo get_field('arquivo', $clipping); ?>" download="">
                <span><i class="icon icon-basic-download"></i> fazer download</span>
            </a>
        </div>
    </div>

    <?php endforeach ?>
    
</div>
<div class="row">
    <div class="col-sm-12 text-center">
        <a href="<?php echo get_term_link(get_field('imprensa_categoria'), 'categoria_imprensa'); ?>" class="btn btn-default text-uppercase" style="margin-bottom: 50px;">Acessar todos</a>
    </div>
</div>
<?php endif; ?>