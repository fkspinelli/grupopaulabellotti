<?php define(_BAN_, 'bg3.png'); 
global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

if( strlen($query_string) > 0 ) {
	foreach($query_args as $key => $string) {
		$query_split = explode("=", $string);
		$search_query[$query_split[0]] = urldecode($query_split[1]);
	} // foreach
} //if

$search = new WP_Query($search_query);
get_header(); 
?>
<section>
    <div class="container">
      	<?php paulabellotti_breadcrumbs(); ?>

        <div class="row">
            <div class="col-sm-12">
                <h2>Sua busca por “<b><?php the_search_query(); ?></b>” encontrou <?php echo $wp_query->found_posts; ?> resultados</h2>
                
                <div class="resultado-busca">
                    <!--<h4>// <span>Tratamentos</span></h4> -->
                    <?php if($wp_query->found_posts > 0):  
            	 ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                    <div class="artigo artigo-blog">
                        <div class="row">
                            <div class="row-height">
                                <div class="col-xs-2 col-height col-top">
                                    <div class="foto" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'thumbnail')[0]; ?>);"></div>
                                </div>
                                <div class="col-xs-10 col-height col-top">
                                    <h3><?php the_title(); ?></h3>
                                    <div class="content">    
                                        <p><?php echo wp_strip_all_tags( get_the_content() ); ?>...</p>
                                    </div>
                                    <a href="<?php the_permalink(); ?>" class="link-icon">continuar lendo</a>
                                </div>
                            </div>
                        </div>
                    </div>
	 				<?php endwhile; endif; ?>
                    <p><span>Foi encontrado <?php echo $wp_query->found_posts; ?> resultados</span></p>
                    <div class="pagination-box">
                        <!--<ul class="pagination">
                            <li><a href="#">«</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">»</a></li>
                        </ul>-->
                        <?php  the_posts_pagination( array( 'mid_size'  => 2 ) ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  
<?php get_footer(); ?>