<?php 
/**
* Template Name: Tecnologia
**/
define(_BAN_, 'bg3.jpg'); get_header(); the_post();
?>
<section>
    <div class="container">
       <?php paulabellotti_breadcrumbs(); ?>
        <div class="row">
            <div class="col-sm-7">
                <div>
                    <h2><?php the_title(); ?></h2>
                    <?php the_content(); ?>
                </div>
				<?php $tecs = get_field('tecnologias'); //print_r($tecs); ?>
                <div class="form-group" style="margin: 50px 0 50px;">
                    <select id="select_tec" class="form-control form-border-none cl-a26f8d text-uppercase">
                        <option value="">Tecnologias</option>
                        <?php foreach ($tecs as $tec): ?>
                        <option value="<?php echo get_permalink($tec->ID); ?>"><?php echo $tec->post_title; ?></option>
                    	<?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-5">
               	 <?php include( locate_template('partials/content-social.php', false, false)); ?>
                <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'thumb_square')[0]; ?>" class="img-thumbnail">
            </div>
        </div>
    </div>
</section>
<div style="margin-bottom: 100px;"></div>
<!-- Posts relacionados a tecnologia -->
<?php include( locate_template( 'partials/content-relacionados.php', false, false ) ); ?>  
<?php get_footer(); ?>