<?php define(_BAN_, 'bg1.png'); get_header(); the_post(); include( locate_template( 'partials/content-banner-clinicas.php', false, false ) ); ?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?php paulabellotti_breadcrumbs(); ?>
                <div>
                    <h2><?php the_title(); ?></h2>
                    <?php the_content(); ?> 
                </div>
                <div class="diver danger"></div>
            </div>
        </div>
        <?php get_template_part('partials/content', 'clinicas'); ?>
    </div>
</section>
<?php get_footer(); ?>