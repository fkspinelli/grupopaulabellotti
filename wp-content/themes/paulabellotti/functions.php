<?php 
show_admin_bar(false);

add_theme_support( 'post-thumbnails' );

// Criando padrões extras de recorte de imagens
add_image_size( 'thumb_blog', 288, 175, false );
add_image_size( 'thumb_square', 400, 400, false );

// Menus
add_action( 'after_setup_theme', 'register_my_menu' );
function register_my_menu() {
   register_nav_menus( array(
        'primary' => 'Header Menu',
        'footer_1' => 'Footer Menu 1',
        'footer_2' => 'Footer Menu 2',
        'footer_3' => 'Footer Menu 3',
    ) );
}


// Removendo gestão padrão posts do wordpress
function remove_menus()
{
  remove_menu_page( 'edit.php' ); //Posts
}
add_action( 'admin_menu', 'remove_menus' );

function my_wpcf7_form_elements($html) {
    $text = 'SELECIONE';
    $html = str_replace('<option value="">---</option>', '<option value="">' . $text . '</option>', $html);
    return $html;
}
add_filter('wpcf7_form_elements', 'my_wpcf7_form_elements');


/*function rc_add_cpts_to_search($query) {

  // Check to verify it's search page
  if( is_search() ) {
    // Get post types
    $post_types = get_post_types(array('public' => true, 'exclude_from_search' => false), 'objects');
    $searchable_types = array();
    // Add available post types
    if( $post_types ) {
      foreach( $post_types as $type) {
        $searchable_types[] = $type->name;
      }
    }
   // print_r($searchable_types);
    $query->set( 'post_type', $searchable_types );
  }
  return $query;
}
add_action( 'pre_get_posts', 'rc_add_cpts_to_search' );*/

//Quando der problema no post type acionar esta função
//flush_rewrite_rules();


// Criando post types
add_action( 'init', 'create_post_type' );

function create_post_type() {
  register_post_type( 'banners',
    array(
      'labels' => array(
        'name' => __( 'Banners' ),
        'singular_name' => __( 'Banner' )
      ),
      'menu_position'         => 4,
      'public' => true,
      'has_archive' => true,
      'supports' => array('title','editor','thumbnail')
    )
  );
  register_post_type( 'imprensa',
    array(
      'labels' => array(
        'name' => __( 'Imprensa' ),
        'singular_name' => __( 'Imprensa' )
      ),
      'menu_position'         => 4,
      'public' => true,
      'has_archive' => true,
      'supports' => array('title','thumbnail')
    )
  );

   register_post_type( 'locais',
    array(
      'labels' => array(
        'name' => __( 'Locais' ),
        'singular_name' => __( 'Local' )
      ),
      'menu_position'         => 4,
      'public' => true,
      'has_archive' => true,
      'supports' => array('title','editor')
    )
  );

   register_post_type( 'clinicas',
    array(
      'labels' => array(
        'name' => __( 'Clinicas' ),
        'singular_name' => __( 'Clinica' )
      ),
      'menu_position'         => 5,
      'public' => true,
      'has_archive' => true,
      'supports' => array('title','excerpt','editor','thumbnail')
    )
  );
    register_post_type( 'equipe',
    array(
      'labels' => array(
        'name' => __( 'Equipe' ),
        'singular_name' => __( 'Equipe' )
      ),
      'menu_position'         => 6,
      'public' => true,
      'has_archive' => true,
      'supports' => array('title','excerpt','editor','thumbnail')
    )
  );
    register_post_type( 'blog',
    array(
      'labels' => array(
        'name' => __( 'Blog' ),
        'singular_name' => __( 'Blog' )
      ),
      'taxonomies' => array('categoria_blog','tag_blog'),
      'menu_position'         => 3,
      'public' => true,
      'has_archive' => true,
      'supports' => array('title','excerpt','editor','thumbnail')
    )
  );
    register_post_type( 'tratamentos',
    array(
      'labels' => array(
        'name' => __( 'Tratamento' ),
        'singular_name' => __( 'Tratamento' )
      ),
      'menu_position'         => 7,
      'public' => true,
      'has_archive' => true,
      'supports' => array('title','excerpt','editor','thumbnail')
    )
  );
    register_post_type( 'tecnologia',
    array(
      'labels' => array(
        'name' => __( 'Tecnologias' ),
        'singular_name' => __( 'Tecnologia' )
      ),
      'menu_position'         => 8,
      'public' => true,
      'has_archive' => true,
      'supports' => array('title','editor','thumbnail')
    )
  );
}
// Adicionando Taxonomias
add_action( 'init', 'custom_taxonomy', 0 );

function custom_taxonomy() {
   $labels_notice = array(
    'name'                       => _x( 'Categoria do blog', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Categoria do blog', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Categoria do blog', 'text_domain' )
    );
  
  // Categoria de conveniados
   $args_conv = array(
    'labels'                     => $labels_notice,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
    
  );

  register_taxonomy( 'categoria_blog', array( 'blog' ), $args_conv );


  register_taxonomy('tag_blog',array( 'blog' ),array(
    'hierarchical' => false,
   // 'labels' => $labels,
    'show_ui' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'tag' ),
  ));

  $labels_imp = array(
    'name'                       => _x( 'Categoria de imprensa', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Categoria de imprensa', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Categoria de imprensa', 'text_domain' )
    );
  
  // Categoria de conveniados
   $args_conv = array(
    'labels'                     => $labels_imp,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
    
  );

  register_taxonomy( 'categoria_imprensa', array( 'imprensa' ), $args_conv );

  // categoria de equiepoe
  $labels_equipe = array(
    'name'                       => _x( 'Categoria de equipe', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Categoria de equipe', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Categoria de equipe', 'text_domain' )
    );
  
  // Categoria de conveniados
   $args_equipe = array(
    'labels'                     => $labels_equipe,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
    
  );

  register_taxonomy( 'categoria_equipe', array( 'equipe' ), $args_equipe );

  // categoria de banner
  $labels_ban = array(
    'name'                       => _x( 'Categoria de banner', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Categoria de banner', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Categoria de banner', 'text_domain' )
    );
  
  // Categoria de conveniados
   $args_ban = array(
    'labels'                     => $labels_ban,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
    
  );

  register_taxonomy( 'categoria_banners', array( 'banners' ), $args_ban );

}

// Setando post views
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

// Retornando os post views
function wpb_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}

// Breadcrumbs
function paulabellotti_breadcrumbs() {
       
    // Settings
    $separator          = '';
    $breadcrums_id      = 'breadcrumb';
    $breadcrums_class   = 'breadcrumb';
    $home_title         = 'Home';
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
           
        // Home page
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        //echo '<li class="separator separator-home"> ' . $separator . ' </li>';
           
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="item-current item-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
              //  echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive">' . $custom_tax_name . '</strong></li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
               // echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                   // $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                //echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
              
            } else {
                  
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                   // $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            //echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            //echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
               
            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
           // echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
               
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
       
        echo '</ul>';
           
    }
       
}

function format_date_br($date)
{
  $ano = substr($date, 0, 4);
  $mes = substr($date, 4, 2);
  $dia = substr($date, 6, 2);

  return $date != '' ? $dia.'/'.$mes.'/'.$ano : '--';

}