$(document).ready(function () {
    $("html").niceScroll({
        cursorcolor: "#000",
        autohidemode: false,
        mousescrollstep: 40,
        scrollspeed: 100
    });
        $('#select_tec').change(function(){
            var url = $(this).val();
            
            if(url != ''){
                window.location.href = url;
            }  
            return false; 
        });

    $.stellar({
        horizontalScrolling: false,
        responsive: true
    });

    $('.localizacao .tabs li').css({'width':100 / $('.localizacao .tabs li').size() + '%'});

    $('.action-menu').on('click touchstart', function(){
        if($(window).width() > 768){
            $('.menu').toggle();
        }else{
            $('.menu').show();
            $('.menu .bar-top + .container').toggle();      
        }
    });


    $(".navbar").on('affix.bs.affix', function(){
        // solto
        // $('body > .background').css({'padding-top':$('.navbar + section').offset().top});
    }).on('affix-top.bs.affix', function(){
        // preso
        $('.action-menu').removeClass('active');
        $('.menu').hide();
        $('body > .background').css({'padding-top':'0'});

    });

    $('.navbar .fa-search, .navbar-toggle + .fa-search').click(function(){
        $('.form-search').slideToggle();
    });

    // $(document).scroll(function(){
    //     if($('body').scrollTop() >= $('.menu').offset().top){
    //         $('.navbar').css({'position':'fixed', 'top': '-175px'});
    //     }
    // });


    if($(window).width() < 768){
        $('.localizacao .tabs a').click(function(){
            $('html, body').animate({
              scrollTop: $('.localizacao .tab-content').offset().top - 50
            }, 1000);
        });
    }

    $(window).on('load resize', function(){
        if($(window).width() < 768){
            $('.navbar-nav>li').click(function(){
                $(this).find('ul').toggle();
            });
        }
    });

    var artigoheight = 0;
    $('.posts .artigo.eqh .panel-body').each(function(){
        if($(this).height() > artigoheight){
            artigoheight = $(this).height();
        }
    });
    $('.posts .artigo.eqh .panel-body').css({'min-height':artigoheight});

    var artigoheight = 0;
    $('.posts .artigo.eqh2 .panel-body').each(function(){
        if($(this).height() > artigoheight){
            artigoheight = $(this).height();
        }
    });
    $('.posts .artigo.eqh .panel-body').css({'min-height':artigoheight - 25});



    // https://rudrastyh.com/javascript/get-photos-from-instagram.html
    // Instagram feed
    var token = '359952351.1677ed0.5fda45f80efe42459d8edfdef58059ff',
        num_photos = 4;
     
    $.ajax({
        url: 'https://api.instagram.com/v1/users/self/media/recent',
        dataType: 'jsonp',
        type: 'GET',
        data: {access_token: token, count: num_photos},
        success: function(data){
            console.log(data);
            for( x in data.data ){
                $('.instagram ul').append('<li><a href="'+data.data[x].link+'" target="_blank"><img src="'+data.data[x].images.low_resolution.url+'"></a></li>');
            }
        },
        error: function(data){
            console.log(data);
        }
    });

    $('.videos .list-videos li').click(function(){
        var videoId = $(this).attr('data-video');
        var iframe = $(this).parents('.videos').find('iframe');
        iframe.attr('src', 'https://www.youtube.com/embed/' + videoId);
    });

    $(document).on('click', '.wpcf7-submit', function(){
        alert();
    });



});