<?php define(_BAN_, 'bg3.png'); get_header(); ?>
<section>
    <div class="top-blog">
        <div class="container">
            <div style="position: absolute; width: 100%;">
                <?php paulabellotti_breadcrumbs(); ?>
            </div>
            <div class="row">
                <div class="col-sm-9 col-sm-push-3 text">
                    <h4>Blog</h4>
                    <h5>Questão de Pele</h5>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="posts">
                <?php while(have_posts()): the_post(); ?>
                    <div class="panel panel-default artigo">
                        <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(),'normal')[0]; ?>">
                        <div class="panel-body">
                            <h5><strong><?php echo get_the_date('d F Y'); ?> </strong> <?php echo get_field('clinica')->post_title; ?></h5>
                            <h3><?php the_title(); ?></h3>
                            <?php the_excerpt(); ?>
                        </div>
                        <div class="panel-footer">
                            <a href="<?php the_permalink(); ?>" class="btn btn-default link-icon text-uppercase pull-left">continuar lendo</a>
                        </div>
                    </div>
                <?php endwhile; ?>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-sm-push-3">
                        <a href="#" class="btn btn-danger btn-block text-uppercase" style="margin-bottom: 50px;">ver mais matérias</a>
                    </div>
                </div>
            </div>
            <?php get_template_part('partials/content', 'sidebar'); ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>